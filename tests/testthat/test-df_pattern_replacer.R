context("test-df_pattern_replacer")

test_that("simple example", {
  mydf <- data.frame(mycol1 = c(1, 2),
                     mycol2 = c("'one'", "'two'"),
                     stringsAsFactors = FALSE)
  
  expect_equal("insert into mytable(mycol1, mycol2) values (1, \'one\')", 
               df_pattern_replacer(mydf, "insert into mytable(mycol1, mycol2) values (?, ?)", c("mycol1", "mycol2"))[1])
  expect_equal("insert into mytable(mycol1, mycol2) values (2, \'two\')", 
               df_pattern_replacer(mydf, "insert into mytable(mycol1, mycol2) values (?, ?)", c("mycol1", "mycol2"))[2])
})
