context("test-replace_query")

test_that("simple tests", {
  expect_equal("insert into mytable(mycol) values (myval)", replace_query("insert into ?(?) values (?)", c("mytable", "mycol", "myval")))
  expect_equal("drop table ?", replace_query("drop table ?", c()))
})
